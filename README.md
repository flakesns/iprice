# Iprice Assessment  

## Installation  
In terminal:  
$mkdir your_directory  
$cd your_directory   
$git clone https://gitlab.com/flakesns/iprice.git .  
$composer install  

Then run the app:  
$php console.php iprice hello world > test.txt  
