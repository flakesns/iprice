/** console.php **/
#!/usr/bin/env php
<?php
require_once __DIR__ . '/vendor/autoload.php';

use Symfony\Component\Console\Application;
use Console\IpriceCommand;

$app = new Application('Iprice Assessment', 'v1.0');
$app -> add(new IpriceCommand());
$app -> run();