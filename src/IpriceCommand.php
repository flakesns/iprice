<?php
namespace Console;
 
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Input\InputArgument;

use \Exception;
 
class IpriceCommand extends Command
{
    protected function configure()
    {
        $this->setName('iprice')
            ->setDescription('Iprice Assessment')
			->setHelp('Convert text to upper case, alternate case and export csv format')
            ->addArgument('text', InputArgument::IS_ARRAY | InputArgument::REQUIRED, 'Insert the text');
    }
	
    protected function execute(InputInterface $input, OutputInterface $output)
    {
		$str = $input->getArgument('text');
		$txt = '';
		if (count($str) > 0) {
			$txt .= implode(' ', $str);
		}
		
		try {
			$str_uc = $this->str_uppercase($txt);
			$output->writeln("String to uppercase:");
			$output->writeln($str_uc);
						
			$output->writeln('');
			$output->writeln("String to alternate upper and lower case:");
			$arrStr = str_split($txt);
			$str_alt = $this->str_alternate($arrStr);
			$output->writeln($str_alt);
			
			$output->writeln('');
			if ($this->str_to_csv($arrStr, 'test.csv')) {
				$output->writeln('CSV created!');
			}
			
		} catch (Exception $e){
			$output->writeln($e->getMessage());
        }
		 
		return 1;
    }
	
	protected function str_uppercase($txt) {
		return strtoupper($txt);
	}
	
	protected function str_alternate(array $arrStr) {
		if (count($arrStr) == 0) {
			throw new Exception("Array is empty!");
		}
		
		$i = 0;
		$arr = [];
		foreach ($arrStr as $letter) {

		if ( $i % 2) {
			$arr[] = strtoupper($letter);
		} else {
			$arr[] = strtolower($letter);
		}

		$i++;
		}
		return implode(',', $arr);
	}
	
	protected function str_to_csv(array $list, $outFilename) {
		if ($outFilename == '' ) {
			throw new Exception("Filename required!");
		}
		
		if (strpos($outFilename, 'csv') === false) {
			$outFilename = $outFilename . ".csv";
		}
		
		$file = fopen($outFilename,"w");
		
		if (!fputcsv($file, $list)) {
			throw new Exception("Unable to create csv!");
			return false;
		}

		fclose($file);
		return true;
	}
}